import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;

public class FoodOrder {
    private JPanel root;
    private JLabel topLabel;
    private JButton Menu1;
    private JButton Menu2;
    private JButton Menu3;
    private JButton Menu4;
    private JButton Menu5;
    private JButton Menu6;
    private JLabel OrderItems;
    private JButton CheckOut;
    private JLabel total;
    private JLabel yen;
    private JTextPane OrderMenu;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrder");
        frame.setContentPane(new FoodOrder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    int x = 0;

    void order(String food,int value){


        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?","Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = OrderMenu.getText();
            OrderMenu.setText(currentText + food +"  "+value+" yen\n");

            JOptionPane.showMessageDialog(null, "Order for "+food+" received.");
            x+=value;
            yen.setText(String.valueOf(x)+" yen");
        }
    }

    public FoodOrder() {
        Menu1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("toothpaste gum for pet", 500);

            }
        });
        Menu2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("low-sodium dry cheese",1100);

            }
        });
        Menu3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fillet with chicken flavor",385);

            }
        });
        Menu4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Birthday strawberry cake", 2050);

            }
        });
        Menu5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sweet potato and soy milk mont blanc", 670);
            }
        });
        Menu6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Birthday rare cheesecake", 1958);

            }
        });
        Menu1.setIcon(new ImageIcon(
                this.getClass().getResource("menu1photo.png")));
        Menu2.setIcon(new ImageIcon(
                this.getClass().getResource("menu2photo.png")));
        Menu3.setIcon(new ImageIcon(
                this.getClass().getResource("menu3photo.png")));
        Menu4.setIcon(new ImageIcon(
                this.getClass().getResource("menu4photo.png")));
        Menu5.setIcon(new ImageIcon(
                this.getClass().getResource("menu5photo.png")));
        Menu6.setIcon(new ImageIcon(
                this.getClass().getResource("menu6photo.png")));

        OrderMenu.addComponentListener(new ComponentAdapter() {
        });
        CheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?","Message",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null, "Thank you!! The total price is "+x+"yen");
                    x=0;
                    yen.setText(x + " yen");
                    OrderMenu.setText(null);

                }
            }
        });
    }
}
